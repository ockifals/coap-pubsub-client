# coap-pubsub-subscribe
An improved CoAP PubSub Client

# Instruction

## Preparation

1. Set *__virtualenv__* path on __config.sh__

2. Install dependencies *using pip

```bash
$ ./setup.sh
```

## Run it!
*with lazy loader

- Run subscriber 

```bash
$ ./subscriber
```
