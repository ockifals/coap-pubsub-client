from utils.db import Db


class Sensordatas(Db):
    def __init__(self):
        Db.__init__(self)
        self.table = 'sensordatas'
        self.connect()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def get_all(self):
        try:
            with self.cursor as cursor:
                cursor.execute("SELECT * FROM `" + self.table + "`")
                return self.cursor.fetchall()
        finally:
            self.disconnect()

    def create(self, topicid, value, timestamp):
        try:
            with self.cursor as cursor:
                cursor.execute(
                    "INSERT INTO `" + self.table + "` (`topic_id`, `value`, `timestamp`) "
                                                   "VALUES (%s, %s, %s)",
                    (topicid, value, timestamp)
                )
            self.conn.commit()
        finally:
            self.disconnect()