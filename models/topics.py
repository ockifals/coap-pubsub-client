from utils.db import Db


class Topics(Db):
    def __init__(self):
        Db.__init__(self)
        self.table = 'topics'
        self.connect()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def get_all(self):
        try:
            with self.cursor as cursor:
                cursor.execute("SELECT * FROM `" + self.table + "`")
                return self.cursor.fetchall()
        finally:
            self.disconnect()

    def get_byid(self, topicid):
        try:
            with self.cursor as cursor:
                cursor.execute("SELECT * FROM `" + self.table + "` WHERE `id`=%s", (topicid,))
                return self.cursor.fetchone()
        finally:
            self.disconnect()

    def create(self, name):
        try:
            with self.cursor as cursor:
                cursor.execute(
                    "INSERT INTO `" + self.table + "` (`name``) VALUES (%s)",
                    (name,)
                )
            self.conn.commit()
        finally:
            self.disconnect()