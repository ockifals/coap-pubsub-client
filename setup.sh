#!/bin/bash

### install python module dependencies
### set VENV_PATH di config.sh

source config.sh

source "${VENV_PATH}/bin/activate"

pip install -r requirements.txt