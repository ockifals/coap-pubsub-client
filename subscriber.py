import asyncio
import json
from threading import Thread

import sys
from aiocoap import Context, Message
from aiocoap.numbers.codes import Code

from models.sensordatas import Sensordatas
from settings import BROKER_HOST


class ResourceTread(Thread):

    def __init__(self, threadid, name):
        Thread.__init__(self)
        self.threadId = threadid
        self.name = name

    def run(self):
        print("Starting " + self.name)

        if 'temp' == self.name:
            callback = temp()
        else:
            callback = co()

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(callback)
        loop.run_until_complete(future)
        print("Exiting " + self.name)


async def temp():
    protocol = await Context.create_client_context()

    request = Message(code=Code.GET, uri='coap://' + BROKER_HOST + '/temp', observe=0)

    pr = protocol.request(request)

    # Note that it is necessary to start sending
    r = await pr.response
    print("Temp First response: %r" % r.payload)

    async for r in pr.observation:
        payload = json.loads(r.payload, encoding='ascii')
        if payload.get('topic_id'):
            sensordatas_model = Sensordatas()
            sensordatas_model.create(
                payload.get('topic_id'),
                payload.get('value'),
                payload.get('timestamp')
            )
        print("Temp Next result: %r" % r.payload)


async def co():
    protocol = await Context.create_client_context()

    request = Message(code=Code.GET, uri='coap://' + BROKER_HOST + '/co', observe=0)

    pr = protocol.request(request)

    # Note that it is necessary to start sending
    r = await pr.response
    print("Co First response: %r" % r.payload)

    async for r in pr.observation:
        payload = json.loads(r.payload, encoding='ascii')
        if payload.get('topic_id'):
            sensordatas_model = Sensordatas()
            sensordatas_model.create(
                payload.get('topic_id'),
                payload.get('value'),
                payload.get('timestamp')
            )
        print("Co Next result: %r" % r.payload)


def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    print("Exiting Main Thread")
    sys.exit(0)


if __name__ == "__main__":
    # signal.signal(signal.SIGINT, signal_handler)

    thread1 = ResourceTread(1, 'temp')
    thread2 = ResourceTread(2, 'co')

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()