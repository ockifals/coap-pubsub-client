import pymysql

MARIADB_HOST = 'localhost'
MARIADB_USER = 'ockifals'
MARIADB_PASSWD = 'admin123'
MARIADB_DBNAME = 'coap_subs'


class Db:
    def __init__(self):
        self.conn = None
        self.cursor = None

    def connect(self):
        self.conn = pymysql.connect(
            host=MARIADB_HOST,
            user=MARIADB_USER,
            password=MARIADB_PASSWD,
            db=MARIADB_DBNAME,
            cursorclass=pymysql.cursors.DictCursor
        )
        self.cursor = self.conn.cursor()

    def disconnect(self):
        self.conn.close()
